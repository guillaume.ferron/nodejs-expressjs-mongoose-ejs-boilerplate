const ejs = require('ejs');
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
session = require('express-session')

module.exports = function(app) {

	app.set('views', __dirname + '/public');
	app.engine('html', require('ejs').renderFile);
	app.set('view engine', 'html');

	app.use(cookieParser())

	app.use(session({
		  secret: 'Nul vainqueur ne croit au hasard',
		  resave: false,
		  saveUninitialized: true,
		  cookie: { secure: true }
	}))

	app.use( bodyParser.json() );       // to support JSON-encoded bodies
	app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
		extended: true
	})); 


}
