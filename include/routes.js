const express = require('express');

module.exports = function(app,auth) {

	//app.use(express.static('/public'))

	app.get('/', (req, res, next) => {
		if(!(req.session && req.session.user)) {
			res.render('index');
		} else {
			res.render('../client/index');
		}
	});

	app.get('/register', auth.isNotLogged, (req, res, next) => {
		res.render('register');
	});

	app.get('/login', auth.isNotLogged, (req, res, next) => {
		res.render('login');
	});

	app.get('/logout', auth.isLogged, (req, res, next) => {
		res.render('logout');
	});

	app.get('/member', auth.isLogged, (req, res, next) => {
		res.render('../client/index');
	});

	app.post('/register', auth.isNotLogged, auth.register, (req, res, next) => {
		res.render('../client/index');
	});

	app.post('/login', auth.isNotLogged, auth.login, (req, res, next) => {
		res.render('../client/index');
	});

	app.post('/logout', auth.isLogged, auth.logout, (req, res, next) => {
		res.render('login');
	});

}
