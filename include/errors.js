module.exports = function(app) {

	// catch 404 and forward to error handler
	app.use(function (req, res, next) {
		var err = new Error('Not Found');
		err.status = 404;
		next(err);
	});


	app.use(function (err, req, res, next) {

		const code = err.status || 500;
		console.log(err);
		res.status(code);
		res.render(code+'.html');

	});

}
