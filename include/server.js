const https = require('https');
const http = require('http');
const fs = require('fs');

const options = {
	key: fs.readFileSync('keyring/localhost.key'),
	cert: fs.readFileSync('keyring/localhost.crt')
};

module.exports = function(app,port,sslport) {

	http.createServer(function (req, res) {
		res.writeHead(301, { "Location": "https://" + req.headers['host'].split(':')[0] + ':' + sslport + req.url });
		res.end();
	}).listen(port);
	console.log('Http server running at '+port);


	https.createServer(options, app).listen(sslport);
	console.log('Secure server running at '+sslport);


}

