const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/aapp', { useNewUrlParser: true });
const db = mongoose.connection;

mongoose.set('useCreateIndex', true);

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
	console.log("Mongo is connected.")
});


const User = mongoose.model('Users', mongoose.Schema({
	email: {
		type: String,
		required: true,
		minlength: 5,
		maxlength: 255,
		unique: true
	},
	password: {
		type: String,
		required: true,
		minlength: 5,
		maxlength: 1024
	}
}));

module.exports = {

	register : function(req, res, next) {

		var user = new User({
			email: req.body.email,
			password: req.body.password
		});

		User.create(user, function(err, newUser) {
			if(err) return next(err);
			req.session.user = req.body.email;
			next();
		});

	},

	login : function(req, res, next) {

		return User.findOne({email: req.body.email, password: req.body.password}, function(err, user) {
			if(err) return next(err);
			if(!user) return res.redirect('/');
			req.session.user = req.body.email;
			next();
		});

	},

	logout : function(req, res, next) {

		req.session.user = null;
		next();

	},

	isLogged : function(req, res, next) {
		if (!(req.session && req.session.user)) {
			return res.redirect('/login');
		}
		next();
	},

	isNotLogged : function(req, res, next) {
		if ((req.session && req.session.user)) {
			return res.redirect('/');
		}
		next();
	}


}

