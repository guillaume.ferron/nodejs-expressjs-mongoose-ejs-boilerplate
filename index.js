const server = require('./include/server.js');
const app = require('./include/app.js');
const config = require('./include/config.js');
const routes = require('./include/routes.js');
const errors = require('./include/errors.js');
const security = require('./include/security.js');
const auth = require('./include/auth.js');
const api = require('./api.js');

config(app);
security(app);
routes(app,auth);
api(app,auth);
errors(app);
server(app,8000,8443);

