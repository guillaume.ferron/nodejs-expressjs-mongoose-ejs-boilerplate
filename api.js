const methods = require('./methods');


module.exports = function(app, auth) {

	app.get('/test-everything', auth.isLogged, (req, res, next) => {

		const result = methods.testEverything();

		res.send({
			api : 'test-everything',
			result : result
		})

	});

}

